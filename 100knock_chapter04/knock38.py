#coding: utf-8
#38. ヒストグラム

import re
import codecs

#形態素解析結果（neko.txt.mecab）を読み込む
morpheme_list = []
with codecs.open('neko.txt.mecab', 'r', 'utf-8') as fin:
	for line in fin:
		morpheme_dict = {}
		if line.find('EOS') >= 0:	#EOSの場合は何もしない
			pass
		else:
			split_line = re.split(r'[\t,]', line)			#タブとカンマで分割
			morpheme_dict.update({"surface":split_line[0]})	#表層系(surface)
			morpheme_dict.update({"base":split_line[7]})	#原型(base)
			morpheme_dict.update({"pos":split_line[1]})		#品詞(pos)
			morpheme_dict.update({"pos1":split_line[2]})	#品詞分類1(pos1)
			morpheme_list.append(morpheme_dict)				#辞書をリストに追加
			

#----------単語の出現頻度のヒストグラムを描く---------
from collections import Counter
import numpy
import pylab

word_list = []
for morpheme in morpheme_list:
	word_list.append(morpheme['surface'])

i = 1
temp_cnt = -1
freq_dict = {}
counter = Counter(word_list)
for word, cnt in counter.most_common():
    if temp_cnt != cnt:
    	freq_dict.update({cnt:i})
    	i = 1
    	temp_cnt = cnt
    else:
		i += 1
X = freq_dict.keys()
Y = freq_dict.values()
pylab.bar(X, Y, align="center")
pylab.show()
