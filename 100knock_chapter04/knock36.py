#coding: utf-8
#36. 単語の出現頻度

import re
import codecs

#形態素解析結果（neko.txt.mecab）を読み込む
morpheme_list = []
with codecs.open('neko.txt.mecab', 'r', 'utf-8') as fin:
	for line in fin:
		morpheme_dict = {}
		if line.find('EOS') >= 0:	#EOSの場合は何もしない
			pass
		else:
			split_line = re.split(r'[\t,]', line)			#タブとカンマで分割
			morpheme_dict.update({"surface":split_line[0]})	#表層系(surface)
			morpheme_dict.update({"base":split_line[7]})	#原型(base)
			morpheme_dict.update({"pos":split_line[1]})		#品詞(pos)
			morpheme_dict.update({"pos1":split_line[2]})	#品詞分類1(pos1)
			morpheme_list.append(morpheme_dict)				#辞書をリストに追加
			

#----------文章中に出現する単語とその出現頻度を求め，出現頻度の高い順に並べる---------
from collections import Counter

word_list = []
for morpheme in morpheme_list:
	word_list.append(morpheme['surface'])
	
counter = Counter(word_list)
for word, cnt in counter.most_common():
    print (word + ":" + str(cnt))

