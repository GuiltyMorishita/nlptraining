#coding: utf-8
#51. 単語の切り出し

import re
from knock50 import makeLines

if __name__ == '__main__':
    lines = makeLines('nlp.txt')
    for line in lines:
        for word in re.split(' ', line.rstrip()):
            print(word)
        print()