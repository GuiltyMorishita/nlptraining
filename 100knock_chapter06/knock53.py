#coding: utf-8
#53. Tokenization

import re
import pprint
import codecs
import json
from corenlp import StanfordCoreNLP
import xml.etree.ElementTree as ET



"""
def ifword(json_obj, line_padding=""):
	result_list = list()
	
	for i, sub_elem in enumerate(json_obj):
		if i == 0:
			result_list.append("%s<word>" % (line_padding))
			result_list.append(json2xml(sub_elem, "\t" + line_padding))
			result_list.append("%s</word>" % (line_padding))
		else:
			result_list.append(json2xml(sub_elem, line_padding))
	
	return "\n".join(result_list)



def ifwords(json_obj, line_padding=""):
	result_list = list()
	
	json_obj_type = type(json_obj)

	if json_obj_type is list:
		for sub_elem in json_obj:
			result_list.append(ifword(sub_elem, line_padding))
		
		return "\n".join(result_list)

	if json_obj_type is dict:
		for tag_name in json_obj:
			if tag_name == "":
				continue
				
			sub_obj = json_obj[tag_name]
			
			if tag_name == "words":
				sub_obj = json_obj[tag_name]
				pprint.pprint(sub_obj)
				continue
			
			result_list.append("%s<%s>" % (line_padding, tag_name))
			result_list.append(json2xml(sub_obj, "\t" + line_padding))
			result_list.append("%s</%s>" % (line_padding, tag_name))
	
		return "\n".join(result_list)

	return "%s%s" % (line_padding, json_obj)


def json2xml(json_obj, line_padding=""):
	result_list = list()
	
	json_obj_type = type(json_obj)

	if json_obj_type is list:
		for sub_elem in json_obj:
			result_list.append(json2xml(sub_elem, line_padding))
		
		return "\n".join(result_list)

	if json_obj_type is dict:
		for tag_name in json_obj:
			if tag_name == "":
				continue
				
			sub_obj = json_obj[tag_name]
			
			if tag_name == "words":
				result_list.append("%s<%s>" % (line_padding, tag_name))
				result_list.append(ifwords(sub_obj, "\t" + line_padding))
				result_list.append("%s</%s>" % (line_padding, tag_name))
				continue
			
			result_list.append("%s<%s>" % (line_padding, tag_name))
			result_list.append(json2xml(sub_obj, "\t" + line_padding))
			result_list.append("%s</%s>" % (line_padding, tag_name))
	
		return "\n".join(result_list)
	
	if json_obj == "<":
		json_obj = json_obj.replace('<', '&lt;')
	return "%s%s" % (line_padding, json_obj)
	"""

if __name__ == '__main__':
	"""
	# パーサの生成
	corenlp_dir = "./stanford-corenlp-full-2013-06-20/"
	properties_file = "./user.properties"
	corenlp = StanfordCoreNLP(corenlp_path=corenlp_dir, properties=properties_file)
	
	xml_list = list()
	with codecs.open('nlp.txt', 'r', 'utf-8') as file_in:
		for line in file_in:
			if line == "\n":
				continue
			else:
				result_json = json.loads(corenlp.parse(line))
				#1行をXML形式に変換
				xml_list.append(json2xml(result_json))

	with codecs.open("nlp.xml", 'w', 'utf-8') as file_out:
		file_out.write("<nlp>\n")
		for xml in xml_list:
			file_out.write(xml)
		file_out.write("\n</nlp>")
	
	"""
	#XMLファイル読み込み
	tree = ET.parse('nlp.txt.xml')
	root = tree.getroot()
	
	#1行1単語の形式で出力
	for elem in root.findall(".//word"):
		word = re.sub('[\t\n]', '', elem.text)
		print(word)
