#coding: utf-8
#57. 係り受け解析

import re
import pprint
import codecs
import xml.etree.ElementTree as ET
	


if __name__ == '__main__':

	#XMLファイル読み込み
	tree = ET.parse('nlp.txt.xml')
	root = tree.getroot()
	
	counter = 0
	dotStr = """digraph {\n"""
	for element in root.findall(".//dependencies"):
		if element.items() == [('type', 'collapsed-dependencies')]:
			if counter !=2:
				counter += 1
				continue
			
			for elem in element:
				govenor_flag = False
				arrow_flag = False
				dependent_flag = False
				for e in elem:
					if e.tag == "governor":
						dotStr = dotStr + "\t" +  "\"" + e.text + "\""
						governor_flag = True
						
					if governor_flag is True:
						dotStr = dotStr + " -> "
						governor_flag = False
						arrow_flag = True
						
					if arrow_flag is True and e.tag == "dependent":
						dotStr = dotStr + "\"" + e.text + "\""
						dependent_flag = True
					
					if dependent_flag is True:
						dotStr = dotStr + ";\n"
		
			counter += 1
		
	dotStr = dotStr + "}"			
	
	with open("knock57_output.dot", 'w') as FileOut:
		FileOut.write(dotStr)
