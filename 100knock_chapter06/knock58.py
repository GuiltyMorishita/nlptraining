#coding: utf-8
#58. タプルの抽出

import re
import pprint
import codecs
import xml.etree.ElementTree as ET
	

if __name__ == '__main__':

	#XMLファイル読み込み
	tree = ET.parse('nlp.txt.xml')
	root = tree.getroot()

	for element in root.findall(".//dependencies"):
		if element.items() == [('type', 'collapsed-dependencies')]:
			nsubj_list = list()
			for elem in element:
				if elem.items() == [('type', 'nsubj')]:
					for e in elem:
						if e.tag == "governor":
							pred = e.text
						
						if e.tag == "dependent":
							subj = e.text
					nsubj_list.append((pred, subj))
							
			dobj_list = list()
			for elem in element:
				if elem.items() == [('type', 'dobj')]:
					for e in elem:
						if e.tag == "governor":
							pred = e.text
						
						if e.tag == "dependent":
							subj = e.text
					dobj_list.append((pred, subj))
			
			for psubj, ssubj in nsubj_list:
				for pobj, sobj in dobj_list:
					if psubj == pobj:
						print(ssubj + "\t" + psubj + "\t" + sobj)
					
