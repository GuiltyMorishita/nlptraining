#coding: utf-8
#52. ステミング

import re
from knock50 import makeLines
from stemming.porter2 import stem

if __name__ == '__main__':
    lines = makeLines('nlp.txt')
    for line in lines:
        for word in re.split(' ', line.rstrip()):
            print(word + "\t" + stem(word))
        print()