#coding: utf-8
#55. 固有表現抽出

import re
import pprint
import codecs
import xml.etree.ElementTree as ET

if __name__ == '__main__':
	
	#XMLファイル読み込み
	tree = ET.parse('nlp.xml')
	root = tree.getroot()
	
	#人名を抜き出す
	for elem in root.findall(".//words"):
		word = ""
		for e in elem:
			if e.tag == "word":
				word = re.sub('[\t\n]', '', e.text)
			if word != "" and re.search(r'PERSON', e.text):
				print(word)
				word = ""
