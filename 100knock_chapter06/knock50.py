#coding: utf-8
#50. 文区切り

import re
import codecs

def makeLines(textFileName):
    with codecs.open(textFileName, 'r', 'utf-8') as fileIn:
        lines = []
        for line in fileIn:
            if re.search(r'[\.;:\?!]\s[A-Z]', line):
                matchObjIter = re.finditer(r'[\.;:\?!]\s[A-Z]', line)
                for i, matchOB in enumerate(matchObjIter):
                    endIdx, startIdx  = matchOB.span()
                    if i == 0:
                        lines.append(line[0:endIdx+1] + "\n")
                        start = startIdx
                    else:
                        lines.append(line[start-1:endIdx+1] + "\n")
                        start = startIdx
                lines.append(line[start-1:])
            elif line == '\n':
                continue
            else:
                lines.append(line)
    return(lines)

if __name__ == '__main__':
    lines = makeLines('nlp.txt')
    for line in lines:
        print(line, end="")