#coding: utf-8
#59. S式の解析

import re
import pprint
import codecs
import xml.etree.ElementTree as ET
	

if __name__ == '__main__':

	#XMLファイル読み込み
	tree = ET.parse('nlp.txt.xml')
	root = tree.getroot()

	for element in root.findall(".//parse"):
		mlist = re.finditer(r'\(NP', element.text)
		for m in mlist:
			counter = 0
			NP = str()
			for i in range(m.end() + 1, len(element.text)):
				if counter == 0 and element.text[i] == ")":
					NP = NP + element.text[i]
					break
				
				elif counter >= 1 and element.text[i] == ")":
					NP = NP + element.text[i]
					counter -= 1
				
				elif element.text[i] == "(":
					NP = NP + element.text[i]
					counter += 1
				
				else:
					NP = NP + element.text[i]
			
			print(re.sub(r'\([A-Z]+ *|[\(\)]', "", NP))
			
			

