#coding: utf-8
#54. 品詞タグ付け

import re
import pprint
import codecs
import xml.etree.ElementTree as ET

if __name__ == '__main__':
	
	#XMLファイル読み込み
	tree = ET.parse('nlp.xml')
	root = tree.getroot()
	
	#単語，レンマ，品詞をタブ区切り形式で出力
	for elem in root.findall(".//words"):
		word = ""
		lemma = ""
		pos = ""
		for e in elem:
			if e.tag == "word":
				word = re.sub('[\t\n]', '', e.text)
			elif e.tag == "Lemma":
				lemma = re.sub('[\t\n]', '', e.text)
			elif e.tag == "PartOfSpeech":
				pos = re.sub('[\t\n]', '', e.text)
			else:
				pass
			if word != "" and lemma != "" and pos != "":
				print(word + "\t" + lemma + "\t" + pos)
				word = ""
				lemma = ""
				pos = ""
