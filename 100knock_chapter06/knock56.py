#coding: utf-8
#56. 共参照解析

import re
import pprint
import codecs
import xml.etree.ElementTree as ET

if __name__ == '__main__':
	
	#XMLファイル読み込み
	tree = ET.parse('nlp.txt.xml')
	root = tree.getroot()
	
	#sentenceのリストを作る
	sentence = str()
	sentence_list = list()
	for elem in root.findall(".//token"):
		for e in elem:
			if e.tag == "word":
				if e.text == ".":
					sentence = sentence + e.text
					sentence_list.append(sentence.lstrip())
					sentence = ""
				
				elif e.text == ",":
					sentence = sentence + e.text
				
				else:
					sentence = sentence + " " + e.text
	
	counter = 0
	mentions = list()
	repsentative_mentions = list()
	for elem in root.findall(".//mention"):
		if elem.items() == [('representative', 'true')]:
			repsentative_mentions.append(elem.find(".//text").text)
			counter += 1
		
		else:
			mentions.append((counter, int(elem.find(".//sentence").text), elem.find(".//text").text))
	
	
	result_sentence_list = list()
	for (i, sentence) in enumerate(sentence_list):
		for (re_men_No, men_No, mention) in mentions:
			if men_No - 1 == i:
				replace_string = repsentative_mentions[re_men_No - 1] + "(" + mention + ")"
				sentence = sentence.replace(mention, replace_string)
		result_sentence_list.append(sentence)	
	for sentence in result_sentence_list:
		print(sentence)
