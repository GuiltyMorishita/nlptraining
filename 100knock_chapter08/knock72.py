#coding: utf-8
#72. 素性抽出

from automain import *
from knock71 import isIncludedStopWord
from stemming.porter2 import stem
import codecs

@automain
def main():
    stopWordList = list()
    #ストップリストの作成
    with codecs.open('./stopword.txt', 'r', 'utf-8') as txtFileIn:
        for stopWord in txtFileIn:
            stopWordList.append(stopWord)

    featureList = list()
    #素性リストの作成
    with codecs.open('./rt-polaritydata/rt-polarity.pos', 'r', 'latin-1') as txtFileIn:
        for line in txtFileIn:
            splitLineList = line.split(" ")
            for word in splitLineList:
                if isIncludedStopWord(stopWordList, word):
                    continue
                else:
                    featureList.append(stem(word))

    #素性リストの出力
    with codecs.open('./feature.pos', 'w', 'utf-8') as txtFileIn:
        for feature in featureList:
            txtFileIn.write(feature + "\n")

    featureList = list()
    #素性リストの作成
    with codecs.open('./rt-polaritydata/rt-polarity.neg', 'r', 'latin-1') as txtFileIn:
        for line in txtFileIn:
            splitLineList = line.split(" ")
            for word in splitLineList:
                if isIncludedStopWord(stopWordList, word):
                    continue
                else:
                    featureList.append(stem(word))

    #素性リストの出力
    with codecs.open('./feature.neg', 'w', 'utf-8') as txtFileIn:
        for feature in featureList:
            txtFileIn.write(feature + "\n")
