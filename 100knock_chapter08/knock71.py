#coding: utf-8
#71. ストップワード

from automain import *
import codecs

def isIncludedStopWord(stopWordList, word):
    for stopWord in stopWordList:
        if word in stopWord:
            return True
    return False

@automain
def main():
    stopWordList = list()
    #ストップリストの作成
    with codecs.open('./stopword.txt', 'r', 'utf-8') as txtFileIn:
        for stopWord in txtFileIn:
            stopWordList.append(stopWord)

    print(isIncludedStopWord(stopWordList, "is")) #True
