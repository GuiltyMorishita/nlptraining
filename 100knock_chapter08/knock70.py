#coding: utf-8
#70. データの入手・整形

from automain import *
import codecs
import random

@automain
def main():
    concatenateLineList = list()

    #rt-polarity.posの各行の先頭に"+1 "という文字列を追加する
    with codecs.open('./rt-polaritydata/rt-polarity.pos', 'r', 'latin-1') as posFileIn:
        for posLine in posFileIn:
            concatenateLineList.append("+1 " + posLine)

    #rt-polarity.negの各行の先頭に"-1 "という文字列を追加する
    with codecs.open('./rt-polaritydata/rt-polarity.neg', 'r', 'latin-1') as negFileIn:
        for negLine in negFileIn:
            concatenateLineList.append("-1 " + negLine)

    random.shuffle(concatenateLineList) #行をランダムにシャッフル

    #sentiment.txtを作成
    with codecs.open('./sentiment.txt', 'w', 'utf-8') as txtFileIn:
        for concLine in concatenateLineList:
            txtFileIn.write(concLine)

    #正例（肯定的な文）の数と負例（否定的な文）の数を出力
    with codecs.open('./sentiment.txt', 'r', 'utf-8') as txtFileIn:
        posCounter = 0
        negCounter = 0
        for sentiLine in txtFileIn:
            if sentiLine.startswith("+1 "):
                posCounter += 1
            elif sentiLine.startswith("-1 "):
                negCounter += 1

        print("Positive:" + str(posCounter))
        print("Negative:" + str(negCounter))
