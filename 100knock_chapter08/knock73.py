#coding: utf-8
#72. 素性抽出

from automain import *
from knock71 import isIncludedStopWord
from stemming.porter2 import stem
import codecs

@automain
def main():
    stopWordList = list()
    #ストップリストの作成
    with codecs.open('./stopword.txt', 'r', 'utf-8') as txtFileIn:
        for stopWord in txtFileIn:
            stopWordList.append(stopWord)

    featureList = list()
    #素性リストの作成
    with codecs.open('./rt-polaritydata/rt-polarity.pos', 'r', 'utf-8') as txtFileIn:
        for line in txtFileIn:
            splitLineList = line.split(" ")
            for word in splitLineList:
                if isIncludedStopWord(stopWordList, word):
                    continue
                elif word == "+1" or word == "-1":
                    continue
                else:
                    featureList.append(stem(word))

    with codecs.open('./result.txt', 'w', 'utf-8') as txtFileIn:
        for feature in featureList:
            txtFileIn.write(feature + "\n")
