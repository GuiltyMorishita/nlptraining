#coding: utf-8
#45. 動詞の格パターンの抽出

import codecs
from knock41 import makeChunksList

if __name__ == '__main__':
	chunksList = makeChunksList('neko.txt.cabocha')
	result = ""
	for i, chunks in enumerate(chunksList):
		if i >= 20:
			break
		for chunk in chunks:
			for morphPre in chunk.morphs:
				counter = 0
				if morphPre.pos == "動詞":
					for src in chunk.srcs:
						for morphPart in chunks[src].morphs:
							if morphPart.pos == "助詞":
								if counter == 0:
									result = result + morphPre.base + "\t"
									counter = 1
								result = result + morphPart.base + " "
					if counter == 1:
						result = result.rstrip() + "\n"

	with codecs.open("knock45_output.txt", 'w', 'utf-8') as FileOut:
		FileOut.write(result)