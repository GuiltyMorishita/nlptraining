#coding: utf-8
#40. 係り受け解析結果の読み込み（形態素）

import re
import codecs


class Morph:
    def __init__(self, surface, base, pos, pos1):   # コンストラクタ
        self.surface = surface
        self.base = base
        self.pos = pos
        self.pos1 = pos1


def makeMorphList(cabochaFileName):
    with codecs.open(cabochaFileName, 'r', 'utf-8') as FileIn:
        counter = 0
        morphList = []
        MorphClassList = []
        for line in FileIn:
            if re.match(r'\* ', line) != None:  #先頭が"* "だったら何もしない
                continue

            if "EOS" in line and counter == 1:  #EOSがに連続続いた場合何もしない
                continue

            if "EOS" in line:   #EOSが来たら形態素のリストを１文のリストに追加
                morphList.append(MorphClassList)
                MorphClassList = []
                counter = 1
                continue

            buf = line.split('\t')
            features = buf[1].split(',')
            MorphClassList.append(Morph(buf[0],features[6],features[0],features[1]))
            counter = 0
    return morphList

if __name__ == '__main__':
    morphList = makeMorphList('neko.txt.cabocha')
    for morphObject in morphList[2]:
        print(morphObject.surface)
