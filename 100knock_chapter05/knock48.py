#coding: utf-8
#48. 名詞から根へのパスの抽出

import codecs
from knock41 import makeChunksList

def extraNounToRootPass(result, chunks, chunk):
    for morph in chunk.morphs:
        if morph.pos == "記号":
            continue
        result = result + morph.surface
    if chunk.dst >= 0:
        result = result + " -> "
        return extraNounToRootPass(result, chunks, chunks[chunk.dst])
    else:
        return result + "\n"

if __name__ == '__main__':
    chunksList = makeChunksList('neko.txt.cabocha')
    result = ""
    for i, chunks in enumerate(chunksList):
        if i >= 10:
            pass
        for chunk in chunks:
            isNoun = False
            for morph in chunk.morphs:
                if morph.pos == "名詞":
                    isNoun = True
                    break
            if isNoun == True and chunk.dst >= 0: #名詞が含まれていたら
                result = extraNounToRootPass(result, chunks, chunk)

    with codecs.open("knock48_output.txt", 'w', 'utf-8') as FileOut:
        FileOut.write(result)