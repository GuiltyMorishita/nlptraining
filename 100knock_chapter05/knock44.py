#coding: utf-8
#44. 係り受け木の可視化

from knock41 import makeChunksList
import codecs

def makeDotFile(chunks):
	dotStr = """digraph {"""
	flag = False
	for chunk in chunks:
		for morphObject in chunk.morphs:    #係り元
			if chunk.dst == -1 or morphObject.pos == "記号":
				continue
			dotStr = dotStr + morphObject.surface
			flag = True
		if chunk.dst >= 0 and morphObject.pos != "記号":
			dotStr = dotStr + " -> "

		for morphObject in chunks[chunk.dst].morphs:    #係り先
			if chunk.dst == -1 or morphObject.pos == "記号" or flag == False:
				continue
			dotStr = dotStr + morphObject.surface

		if chunk.dst >= 0 and flag == True:
			dotStr = dotStr + ";"
	dotStr = dotStr + "}"
	print(dotStr)

	with codecs.open("knock44_output.dot", 'w', 'utf-8') as FileOut:
		FileOut.write(dotStr)

if __name__ == '__main__':
	chunksList = makeChunksList('neko.txt.cabocha')
	makeDotFile(chunksList[3])
