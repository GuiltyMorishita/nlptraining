#coding: utf-8
#46. 動詞の格フレーム情報の抽出

import codecs
from knock41 import makeChunksList

if __name__ == '__main__':
	chunksList = makeChunksList('neko.txt.cabocha')
	result = ""
	for i, chunks in enumerate(chunksList):
		if i >= 20:
			pass
		for chunk in chunks:
			for morphPre in chunk.morphs:
				counter = 0
				clauses = []
				if morphPre.pos == "動詞":
					for src in chunk.srcs:
						morphList = []
						isParticle = False
						for morphPart in reversed(chunks[src].morphs):
							if morphPart.pos == "助詞":
								isParticle = True
							if isParticle == True:
								morphList.append(morphPart.surface)
								if counter == 0:
									result = result + morphPre.base + "\t"
									counter = 1
								if morphPart.pos == "助詞":
									result = result + morphPart.base + " "
						if morphList != []:
							clauses.append("".join(reversed(morphList)))
					if counter == 1:
						result = result.rstrip() + "\t"
						for clause in clauses:
							result = result + clause + " "
						result = result.rstrip() + "\n"

	with codecs.open("knock46_output.txt", 'w', 'utf-8') as FileOut:
		FileOut.write(result)