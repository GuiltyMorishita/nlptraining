#coding: utf-8
#43. 名詞を含む文節が動詞を含む文節に係るものを抽出

from knock41 import makeChunksList

if __name__ == '__main__':
    chunksList = makeChunksList('neko.txt.cabocha')
    for chunks in chunksList:
        for chunk in chunks:
            isNoun = False
            isVerb = False

            #係り元の文節に名詞を含まれているか調べる
            for morphObject in chunk.morphs:
                if chunk.dst == -1 or morphObject.pos == "記号":
                    continue
                #名詞が含まれていたらisNounをTrueにする
                if morphObject.pos == "名詞":
                    isNoun = True
                    #係り先の文節に動詞が含まれているか調べる
                    for morphObject in chunks[chunk.dst].morphs:
                        if morphObject.pos == "記号":
                            continue
                        if morphObject.pos == "動詞":
                            isVerb = True
                            break
                    break
            
            #名詞を含む文節が動詞を含む動詞にかかっていた場合
            if isNoun == True and isVerb == True:
                for morphObject in chunk.morphs:    #係り元の出力
                    if chunk.dst == -1 or morphObject.pos == "記号":
                        continue
                    print(morphObject.surface, end="")

                if chunk.dst >= 0 and morphObject.pos != "記号":
                    print("\t", end="")

                for morphObject in chunks[chunk.dst].morphs:    #係り先の出力
                    if chunk.dst == -1 or morphObject.pos == "記号":
                        continue
                    print(morphObject.surface, end="")

                if chunk.dst >= 0:
                    print()
