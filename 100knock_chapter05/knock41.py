#coding: utf-8
#41. 係り受け解析結果の読み込み（文節・係り受け）

import re
import codecs
from knock40 import Morph

class Chunk:
    def __init__(self, morphs, dst):  #コンストラクタ
        self.morphs = morphs    #形態素（Morphオブジェクト）のリスト
        self.dst = int(dst)          #係り先文節インデックス番号
        self.srcs = []        #係り元文節インデックス番号のリスト

    def setSrcs(self, srcs):
        self.srcs = srcs

def makeChunksList(cabochaFileName):
    with codecs.open(cabochaFileName, 'r', 'utf-8') as FileIn:
        counter = 0
        MorphList = []
        ChunkList = []
        ChunksList = []
        for line in FileIn:
            if re.match(r'\* ', line) != None:  #先頭が"* "だったら何もしない
                if MorphList == []:
                    buf = line.split(' ')
                    dst = buf[2].replace('D', '')
                    continue
                else:
                    ChunkList.append(Chunk(MorphList, dst))
                    MorphList = []
                    buf = line.split(' ')
                    dst = buf[2].replace('D', '')
                    continue

            if "EOS" in line and counter == 1:  #EOSがに連続続いた場合何もしない
                continue

            if "EOS" in line:   #EOSが来たら形態素のリストを１文のリストに追加
                ChunkList.append(Chunk(MorphList, dst))
                ChunksList.append(ChunkList)
                MorphList = []
                ChunkList = []
                counter = 1
                continue

            buf = line.split('\t')
            features = buf[1].split(',')
            MorphList.append(Morph(buf[0],features[6],features[0],features[1]))
            counter = 0

    for i, chunks in enumerate(ChunksList):
        srcsList = [[] for col in range(len(chunks))]
        for i, chunk in enumerate(chunks):
            if chunk.dst >= 0:
                srcsList[chunk.dst].append(i)
        for i, chunk in enumerate(chunks):
            chunk.setSrcs(srcsList[i])

    return ChunksList

if __name__ == '__main__':
    chunksList = makeChunksList('neko.txt.cabocha')
    for chunk in chunksList[7]:
        for morphObject in chunk.morphs:
            print(morphObject.surface, end="")
        print("\t", end="")
        for morphObject in chunksList[7][chunk.dst].morphs:
            print(morphObject.surface, end="")
        print()