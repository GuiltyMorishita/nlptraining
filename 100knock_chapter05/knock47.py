#coding: utf-8
#47. 機能動詞構文のマイニング

import codecs
from knock41 import makeChunksList

if __name__ == '__main__':
    chunksList = makeChunksList('neko.txt.cabocha')
    result = ""
    for i, chunks in enumerate(chunksList):
        if i >= 300:
            pass
        for chunk in chunks:
            for morphPre in chunk.morphs:
                buff = ""
                counter = 0
                clauses = []
                partList = []
                if morphPre.pos == "動詞":
                    for src in chunk.srcs:
                        morphList = []
                        isWo = False
                        isParticle = False
                        for morphPart in reversed(chunks[src].morphs):
                            if morphPart.pos == "助詞":
                                isParticle = True
                                if morphPart.base == "を":
                                    isWo = True
                                    continue
                            if isWo == True and morphPart.pos == "名詞" and morphPart.pos1 == "サ変接続":
                                buff = morphPart.surface + "を" + morphPre.base
                            if isParticle == True and isWo != True:
                                morphList.append(morphPart.surface)
                                if counter == 0:
                                    #result = result + morphPre.base + "\t"
                                    counter = 1
                                if morphPart.pos == "助詞" and morphPart.pos1 != "終助詞":
                                    partList.append(morphPart.base)
                        if morphList != []:
                            clauses.append("".join(reversed(morphList)))
                    if counter == 1 and buff != "" and partList != []:
                        result = result + buff + "\t"
                        for part in partList:
                            result = result + part + " "
                        result = result.rstrip() + "\t"
                        for clause in clauses:
                            result = result + clause + " "
                        result = result.rstrip() + "\n"

    with codecs.open("knock47_output.txt", 'w', 'utf-8') as FileOut:
        FileOut.write(result)