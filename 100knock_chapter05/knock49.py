#coding: utf-8
#49. 名詞間の係り受けパスの抽出

import codecs
from knock41 import makeChunksList

def extraNounToRootPass(result, chunks, chunk):
    if chunk.dst >= 0:
        result = result + " -> "
    else:
        result = result + " | "
    for morph in chunk.morphs:
        if morph.pos == "記号":
            continue
        else:
            result = result + morph.surface
    if chunk.dst >= 0:
        return extraNounToRootPass(result, chunks, chunks[chunk.dst])
    else:
        return result

def extraNounToRootPass2(result, chunks, chunk, i, j):
    for morph in chunks[j].morphs:
        if morph.pos == "記号":
            continue
        elif chunks[i] == chunk and morph.pos == "名詞":
            result = result + " | Y"
        else:
            result = result + morph.surface
    if chunk.dst >= 0:
        return extraNounToRootPass(result, chunks, chunks[j+1])
    else:
        return result

def extraNounToRootPass49(result, chunks, chunk, i, j):
    for morph in chunk.morphs:
        if morph.pos == "記号":
            continue
        elif chunks[i] == chunk and morph.pos == "名詞":
            result = result + "X"
        elif chunks[j] == chunk and morph.pos == "名詞":
            result = result + "Y"
        else:
            result = result + morph.surface

    if chunk.dst >= 0 and chunk.dst <= j:
        result = result + " -> "
        return extraNounToRootPass49(result, chunks, chunks[chunk.dst], i, j)
    elif chunk.dst >= 0 and chunk.dst == chunks[i].dst:
        return extraNounToRootPass2(result, chunks, chunk, i, j) + "\n"
    else:
        return result + "\n"

if __name__ == '__main__':
    chunksList = makeChunksList('neko.txt.cabocha')
    result = ""
    for cnt, chunks in enumerate(chunksList):
        if cnt >= 1000:
            break
        for i, chunk_i in enumerate(chunks):
            isNounX = False
            for morph in chunk_i.morphs:
                if morph.pos == "名詞" and chunk_i.dst >= 0: #名詞句Xの発見
                    isNounX = True
                    break
            if isNounX is True: #名詞句Xがあった時
                for j, chunk_j in enumerate(chunks):
                    isNounY = False
                    for morph in chunk_j.morphs:
                        if i < j and morph.pos == "名詞" and chunk_j.dst >= 0: #名詞句Xと
                            isNounY = True
                            break
                    if isNounY is True:
                        result = extraNounToRootPass49(result, chunks, chunk_i, i, j)

    with codecs.open("knock49_output.txt", 'w', 'utf-8') as FileOut:
        FileOut.write(result)