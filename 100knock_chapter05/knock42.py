#coding: utf-8
#42. 係り元と係り先の文節の表示

from knock41 import makeChunksList

if __name__ == '__main__':
	chunksList = makeChunksList('neko.txt.cabocha')
	for chunks in chunksList:
		flag = False
		for chunk in chunks:
			for morphObject in chunk.morphs:    #係り元
				if chunk.dst == -1 or morphObject.pos == "記号":
					continue
				print(morphObject.surface, end="")
				flag = True
			if chunk.dst >= 0 and morphObject.pos != "記号":
				print("\t", end="")

			for morphObject in chunks[chunk.dst].morphs:    #係り先
				if chunk.dst == -1 or morphObject.pos == "記号":
					continue
				print(morphObject.surface, end="")

			if chunk.dst >= 0 and flag == True:
				print()
