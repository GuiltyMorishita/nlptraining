#coding: utf-8
#68. ソート

import re
import pprint
import codecs
import gzip
import json
from pymongo import MongoClient

if __name__ == '__main__':

	db_name = 'artist'
	
	connect = MongoClient('127.0.0.1',27017)	
	db = connect.artist
	posts = db.post
	
	dance_dict = dict()
	for data in posts.find({"$and":[{"tags":{"$exists":True}}, {"rating":{"$exists":True}}]}):
		for d in data["tags"]:
			if d["value"] == "dance":
				dance_dict.update({data["rating"]["value"]:data})
				
	counter = 0
	for k, v in sorted(dance_dict.items()):
		if counter < 20:
			print v
	
