#coding: utf-8
#62. KVS内の反復処理

import re
import pprint
import codecs
import gzip
import json
import redis
	

if __name__ == '__main__':

	#JSONファイルを読み込む
	with gzip.open('artist.json.gz', 'rb') as fin:
		all_lines = fin.read()

	r = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
	
	counter = 0
	for area in r.hvals("name_area"):
		if area == "Japan":
			counter += 1
	
	print("artist number:" + str(counter))
