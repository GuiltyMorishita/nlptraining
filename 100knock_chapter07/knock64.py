#coding: utf-8
#64. MongoDBの構築

import re
import pprint
import codecs
import gzip
import json
from pymongo import MongoClient

if __name__ == '__main__':

	db_name = 'artist'
	
	#JSONファイルを読み込む
	with gzip.open('artist.json.gz', 'rb') as fin:
		all_lines = fin.read()

	connect = MongoClient('127.0.0.1',27017)
	connect.drop_database(db_name)
	
	db = connect.artist
	posts = db.post
	
	
	
	#1アーティストごとに分割しRedisへ登録
	for i, line in enumerate(all_lines.splitlines()):
		print i
		if i >= 10:
			pass
		artist_info = json.loads(line)
		posts.insert(artist_info)
