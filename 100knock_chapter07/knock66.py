#coding: utf-8
#66. 検索件数の取得

# >db.post.find({"area":"Japan"}).count()

import re
import pprint
import codecs
import gzip
import json
from pymongo import MongoClient

if __name__ == '__main__':

	db_name = 'artist'
	
	connect = MongoClient('127.0.0.1',27017)	
	db = connect.artist
	posts = db.post
	
	counter = posts.find({"area":"Japan"}).count():	
	print(counter)
