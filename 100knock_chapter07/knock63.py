#coding: utf-8
#63. オブジェクトを値に格納したKVS

import re
import pprint
import codecs
import gzip
import json
import redis

if __name__ == '__main__':

	#JSONファイルを読み込む
	with gzip.open('artist.json.gz', 'rb') as fin:
		all_lines = fin.read()

	r = redis.Redis(host='127.0.0.1', port=6379, db=0)

	#1アーティストごとに分割しRedisへ登録
	"""
	for i, line in enumerate(all_lines.splitlines()):
		if i >= 10:
			pass
		artist_info = json.loads(line)
		if "name" in artist_info:
			if "tags" in artist_info:
				r.hset("name_tags", artist_info["name"],artist_info["tags"])  # <--- key,valueの登録
	"""

	artist_tags = r.hget("name_tags", "Matu")
	mlist = re.finditer(r"count", artist_tags)
	counter = 0
	for m in mlist:
		counter += 1
	print(artist_tags, counter)
