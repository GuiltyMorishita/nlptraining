#coding: utf-8
#67. 複数のドキュメントの取得

import re
import pprint
import codecs
import gzip
import json
from pymongo import MongoClient

if __name__ == '__main__':

	db_name = 'artist'
	
	connect = MongoClient('127.0.0.1',27017)	
	db = connect.artist
	posts = db.post
	
	target_name = "Project Object"
	for data in posts.find({"aliases":{"$exists":True}}):
		if data["aliases"][0]["name"] == target_name:
			print data
