#coding: utf-8
#04. 元素記号

import re
string = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."

word_list = list(filter(lambda w: len(w) > 0, re.split(r'\s|"|,|\.', string)))
dictionary = {}
word_list_len = len(word_list)
for i in range(0, word_list_len):
    if i == 0 or i == 4 or i == 5 or i == 6 or i == 7 or i == 8 or i == 14 or i == 15 or i == 18:
        str_char1 = word_list[i][0]
        dictionary[str_char1] = i + 1
    else:
        str_char2 = word_list[i][0:2]
        dictionary[str_char2] = i + 1
print(dictionary)

