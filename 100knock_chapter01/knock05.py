#coding: utf-8
#05. n-gram

import re

def word_bigram(x):
    word_list = list(filter(lambda w: len(w) > 0, re.split(r'\s|"|,|\.', x)))
    bigram_word = []
    word_list_len = len(word_list)
    for i in range(0, word_list_len - 1):
        if i != word_list_len:
            bigram_word.append(word_list[i] + word_list[i+1])
        else:
            bigram_word.append(word_list[i])
    return(bigram_word)

def char_bigram(x):
    char_list = list(filter(lambda s: s != " ", x))
    bigram_char = []
    char_list_len = len(char_list)
    for i in range(0, char_list_len - 1):
        if i != char_list_len:
            bigram_char.append(char_list[i] + char_list[i+1])
        else:
            bigram_char.append(char_list[i])
    return(bigram_char)

string = "I am an NLPer"
print(word_bigram(string))
print(char_bigram(string))