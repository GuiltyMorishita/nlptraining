#coding: utf-8
#08. 暗号文

def cipher(s):
	char_list = list(filter(lambda s: s != " ", s))
	conv_list = []
	for char in char_list:
		if char.islower():
			conv = chr(219 - ord(char))
			conv_list.append(conv)
		else:
			conv_list.append(char)
	conv_string = "".join(conv_list)
	return(conv_string)

string = "IDFnidgFFGnigteANIfINgdagdsa"
string = cipher(string)
print(string)
string = cipher(string)
print(string)