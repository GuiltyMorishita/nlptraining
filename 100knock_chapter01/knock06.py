#coding: utf-8
#06. 集合

from knock05 import char_bigram

string1 = "paraparaparadise"
string2 = "paragraph"

X = set(char_bigram(string1))
Y = set(char_bigram(string2))

bigram_union = X.union(Y)
print(bigram_union)
bigram_intersection = X.intersection(Y)
print(bigram_intersection)
bigram_difference = X.difference(Y)
print(bigram_difference)

if "se" in X:
    print("Xには'se'が含まれる")
else:
    print("Xには'se'が含まれない")
if "se" in Y:
    print("Yには'se'が含まれる")
else:
    print("Yには'se'が含まれない")
