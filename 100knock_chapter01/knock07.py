#coding: utf-8
#07. テンプレートによる文生成

def string_template(x, y, z):
    temp_string = x + "時の" + y + "は" + z
    return(temp_string)

s1 = "12"
s2 = "気温"
s3 = "22.4"

print(string_template(s1, s2, s3))