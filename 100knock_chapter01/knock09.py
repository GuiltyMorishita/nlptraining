#coding: utf-8
#09. Typoglycemia

import re
import random

string = "I couldn't believe that I could actually understand what I was reading : the phenomenal power of the human mind ."

word_list = list(filter(lambda w: len(w) > 0, re.split(r'\s|"|,|\.', string)))
random_list = []

for word in word_list:
    random_char_list = []
    if len(word) > 4:
        char_list = list(word)
        start_c = char_list[0]
        center_c = char_list[1:-1]
        print(center_c)
        end_c = char_list[-1]
        random.shuffle(center_c)
        random_char_list.append(start_c)
        random_char_list.extend(center_c)
        random_char_list.append(end_c)
        random_word = "".join(random_char_list)
        random_list.append(random_word)
    else:
        random_list.append(word)
random_string =  " ".join(random_list)
print(random_string)