#coding: utf-8
#03. 円周率

import re
string = "Now I need a drink, alcoholic of course, after the heavy lectures involving quantum mechanics."

word_list = re.split(r'[\s",\.]+', string)
pi_list = []
for word in word_list:
    pi_list.append(len(word))
pi_string = "".join(map(str, pi_list))
print(pi_string)