#coding: utf-8
#12. 1列目をcol1.txtに，2列目をcol2.txtに保存

import re
import codecs

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
    with codecs.open('col1.txt', 'w', 'utf-8') as fout1:
         with codecs.open('col2.txt', 'w', 'utf-8') as fout2:
             for line in fin:
                 row_list = re.split(r'[\s]+', line)
                 fout1.write((row_list[0] + "\n"))
                 fout2.write(row_list[1] + "\n")