#coding: utf-8
#18. 各行を3コラム目の数値の降順にソート

import re
import codecs

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
    dict = {}
    for line in fin:
        row_list = re.split(r'[\s\t",]+', line)
        dict.update({line:float(row_list[2])})
    dict = sorted(dict.items(), key=lambda x:x[1])
    for hoge in dict:
        print(hoge[0].rstrip())