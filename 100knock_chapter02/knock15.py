#coding: utf-8
#15. 末尾のN行を出力

import sys
import codecs

n = int(sys.argv[1])

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
     lines = fin.readlines()
     for i in range(len(lines) - n, len(lines)):
         print(lines[i].rstrip())