#coding: utf-8
#17. １列目の文字列の異なり

import re
import codecs

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
    first_row = []
    for line in fin:
        row_list = re.split(r'[\s\t",\.]+', line)
        first_row.append(row_list[0])
    first_row_uniq = list(set(first_row))
    first_row_uniq.sort()
    for s in first_row_uniq:
        print(s)