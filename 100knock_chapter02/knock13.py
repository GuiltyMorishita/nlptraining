#coding: utf-8
#13. col1.txtとcol2.txtをマージ

import re
import codecs

with codecs.open('col1.txt', 'r', 'utf-8') as fin1:
    with codecs.open('col2.txt', 'r', 'utf-8') as fin2:
        with codecs.open('merged.txt', 'w', 'utf-8') as fout:
            lines1 = fin1.readlines()
            lines2 = fin2.readlines()
            merged_list = [x + "\t" + y for (x, y) in zip(lines1, lines2)]
            for merged in merged_list:
                merged = re.sub("\n", "",merged)
                fout.write((merged + "\n"))
