#coding: utf-8
#16. ファイルをN分割する

import sys
import codecs
import itertools

n = int(sys.argv[1])

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
    lines = fin.readlines()
    i = 0
    for split_lines in itertools.zip_longest(*[iter(lines)]*n):
        with codecs.open('split' + str(i + 1) + '.txt', 'w', 'utf-8') as fout:
          for line in split_lines:
              if line is None:
                  break
              fout.write(line)
          i += 1