#coding: utf-8
#11. タブをスペースに置換

import re
import codecs

with codecs.open('hightemp.txt', 'r', 'utf-8') as f:
     for line in f:
         print (re.sub('\t', ' ', line.rstrip()))

