#coding: utf-8
#14. 先頭からN行を出力

import sys
import codecs

n = int(sys.argv[1])

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
     i = 0
     for line in fin:
         if i >= n:
             break
         print(line.rstrip())
         i += 1