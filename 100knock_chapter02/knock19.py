#coding: utf-8
#19. 各行の1コラム目の文字列の出現頻度を求め，出現頻度の高い順に並べる

import re
import codecs

with codecs.open('hightemp.txt', 'r', 'utf-8') as fin:
    first_row = []
    for line in fin:
        row_list = re.split(r'[\s\t",]+', line)
        first_row.append(row_list[0])
    words = {}
    for word in first_row:
        words[word] = words.get(word, 0) + 1

    d = [(v,k) for k,v in words.items()]
    d.sort()
    d.reverse()
    for count, word in d[::]:
        print(count, word)