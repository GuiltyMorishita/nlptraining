#coding: utf-8
#28. MediaWikiマークアップの除去

import gzip
import json
import re


#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

aritcle_of_england = article_dict[u'イギリス']['text']

#--------------------------------------------------------------------
#27の処理に加えて，テンプレートの値からMediaWikiマークアップを可能な限り除去し，国の基本情報を整形する
basic_info_dict = {}
BASIC_INFO_FIND = False
for line in aritcle_of_england.splitlines():
	if line == "}}":	#基礎情報の終端
		BASIC_INFO_FIND = False
	if line.find(u"基礎情報") >= 0:
		BASIC_INFO_FIND = True
	if  BASIC_INFO_FIND == True and line[0] == "|":
		
		#----------その他のMediaWikiマークアップの除去----------
		line = re.sub(r"\[\[[^\|\]]*\|[^\|\]]*\|", "", line)	#[[ファイル:Wiki.png|thumb|説明文]]の除去
		line = re.sub(r"{{[^\|}]*\|[^\|]*\|", "", line)			#{{ }}のやつ除去
		line = re.sub(r"{+", "", line)
		line = re.sub(r"}+", "", line)
		line = re.sub(r"\[http://[^\s]*\s", "", line)			#外部リンク除去
		#----------------------------------------------------
		
		line = re.sub(r"''+", "", line)	#強調マークアップ ' を除去
		#[[記事名|表示文字]]と[[記事名#節名|表示文字]]の | までを除去
		line = re.sub(r"\[\[[^\|\]]*\|", "", line)
		line = re.sub(r"\[+", "", line)	# [[ を除去
		line = re.sub(r"\]+", "", line)	# ]] を除去

		line = re.sub(r"\s=\s", "\t", line)
		split_line = line[1:].split("\t")
		basic_info_dict.update({split_line[0] : split_line[2]})
