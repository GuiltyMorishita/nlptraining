#coding: utf-8
#22. カテゴリ名の抽出

import gzip
import json
import re


#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

aritcle_of_england = article_dict[u'イギリス']['text']

#--------------------------------------------------------------------
#記事のカテゴリ名を（行単位ではなく名前で）抽出
for ctgry in re.findall("\[\[Category:(.*?)\]\]", aritcle_of_england):
	print (ctgry)
