#coding: utf-8
#23. セクション構造

import gzip
import json
import re

#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

aritcle_of_england = article_dict[u'イギリス']['text']

#--------------------------------------------------------------------
#記事中に含まれるセクション名とそのレベル（例えば"== セクション名 =="なら1）を表示
for level,name in re.findall("(==+)(.*?)==", aritcle_of_england):
	print (name + str(len(level)-1))
