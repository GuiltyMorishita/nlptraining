#coding: utf-8
#24. ファイル参照の抽出

import gzip
import json
import re

#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

aritcle_of_england = article_dict[u'イギリス']['text']

#--------------------------------------------------------------------
#記事から参照されているメディアファイルをすべて抜き出す
for ctgry in re.findall("(?:File|ファイル):(.*?)[\|\]]", aritcle_of_england):
	print (ctgry)
