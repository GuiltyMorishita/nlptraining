#coding: utf-8
#20. JSONデータの読み込み

import gzip
import json

#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

#「イギリス」に関する記事本文の表示
aritcle_of_england = article_dict[u'イギリス']['text']
print aritcle_of_england
