#coding: utf-8
#25. テンプレートの抽出

import gzip
import json


#Wikipedia記事のJSONファイルを読み込む
with gzip.open('jawiki-country.json.gz', 'rb') as fin:
	all_lines = fin.read()

#1記事ごとに分割し辞書へ登録
article_dict = {}
for line in all_lines.splitlines():
	line_dict = json.loads(line)
	article_dict[line_dict['title']] = line_dict

aritcle_of_england = article_dict[u'イギリス']['text']

#--------------------------------------------------------------------
#記事中に含まれる「基礎情報」テンプレートのフィールド名と値を抽出し，辞書オブジェクトとして格納する
basic_info_dict = {}
BASIC_INFO_FIND = False
for line in aritcle_of_england.splitlines():
	if line == "}}":	#基礎情報の終端
		BASIC_INFO_FIND = False
	if line.find(u"基礎情報") >= 0:
		BASIC_INFO_FIND = True
	if  BASIC_INFO_FIND == True and line[0] == "|":
		line = re.sub(r"\s=\s", "\t", line)
		split_line = line[1:].split("\t")
		basic_info_dict.update({split_line[0] : split_line[2]})
